import createSocketConnection from 'socket.io-client';
import { fromEvent } from 'rxjs';
import { mergeMap, mapTo } from 'rxjs/operators';
import { flow, curry, constant } from 'lodash';

// connect :: URL -> Stream Connection
export const connect = flow(
    host => createSocketConnection(host, { cors: { origin: '*' }}),
    (io) => fromEvent(io, 'connect').pipe(mapTo(io))
);

// messages ::  Stream Connection -> String -> Stream Message
export const messages = curry((event, connection$) => 
    flow(
        constant(connection$), 
        mergeMap(client => fromEvent(client,event))
    )());

// getRealtimeMetrics :: URL -> Stream Metric
export const getRealtimeMetrics = flow(
    connect,
    messages('metric')
);