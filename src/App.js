
import React, { Component } from 'react';
import logo from './logo.svg';
import ChartD3 from './ChartD3.jsx';
import * as _ from 'lodash';
import './App.css';
import { getRealtimeMetrics } from './realtime-metrics';

class App extends Component {
  constructor(props) {
    super(props);
    this.realtimeMetricsSubscription = null;
    this.state = {
      selected: 1,
      title: 'HubSpot Stock in D3',
      type: 'd3',
      isLoading: true,
      data: []
    };
  }
  componentDidMount(){
    const realtimeMetrics = getRealtimeMetrics(process.env.REACT_APP_REALTIME_METRICS_HOST)

    this.realtimeMetricsSubscription = realtimeMetrics.subscribe(
      (metric) => this.setState({ 
        isLoading: false, 
        data: _.takeRight(this.state.data.concat(metric), 300)
      }),
      () => this.setState({ error: true, isLoading: false })
     );
  }

  componentWillUnmount() {
    this.realtimeMetricsSubscription.unsubscribe();
  }

  render() {
    let content;
    if (this.state.isLoading) {
      content = <div>Loading...</div>
    }else if(this.state.error){
      content = <div>ERROR!</div>
    }else {
      content = <ChartD3 data={this.state.data} title={this.state.title} />;;
    }
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Charts with React.</h2>
        </div>
        <div className="container">
          {content}
        </div>
      </div>
    );
  }
}

export default App;
