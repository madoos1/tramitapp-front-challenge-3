# TramitApp Front-End challenge #3

En esta página, mostramos un gráfico con muchos datos (los datos que hay no tienen sentido, pero imaginemos que fuesen los datos de un eje de un acelerómetro que monitoriza en tiempo real una estructura para ver los datos en caso de un terremoto).
¿Por qué es tan lento la carga del gráfico? ¿Sería viable hacer que un gráfico así se renderizase en tiempo real a través de websockets si la información se actualiza 1 vez por segundo más o menos? Realiza una solución que haga que el gráfico vaya más fluido.

## Iniciar APP

Esta aplicación implementa la solución utilizando websockets, para que funcione es necesario lanzar el servidor de métricas en tiempo real.

* Descargar el servidor de metrícas en tiempo real: `git clone git@bitbucket.org:madoos1/tramitapp-backend-challenge.git`
* `cd tramitapp-backend-challenge`
* Ejecutar el servidor de metrícas en tiempo real: `npm install`
* Ejecutar el servidor de metrícas en tiempo real: `node realtime-metrics.js`
* En la APP front: `npm install`
* En la APP front: `npm start`

## Solución

El app paga un coste alto inicial al descargar un fichero que pesa 19MB y porque intenta renderizar 333,000 registros de forma síncrona. Para solucionar estos problemas se implementa una solución utilizando socketio y rxjs. Para evitar memory leaks se toman 300 métricas para renderizar la gráfica.


```js

  componentDidMount(){
    const realtimeMetrics = getRealtimeMetrics(process.env.REACT_APP_REALTIME_METRICS_HOST)

    this.realtimeMetricsSubscription = realtimeMetrics.subscribe(
      (metric) => this.setState({ isLoading: false, data: this.state.data.concat(metric)}),
      () => this.setState({ error: true, isLoading: false })
     );
  }

```

Ha sido necesario modificar la librería local chartD3.js ya que estaba mutando el estado y llevaba a insistencias.

```js
chart.draw = function(svg, dataState, title) {
  this.cleanUp();
  const data = dataState.map(({ Date, Close }) => ({ Close, Date: parseTime(Date) })) // change to immutable to avoid state changes
  ...
}

```

Se puede ver la implementación del método getRealtimeMetrics en el fichero src/realtime-metrics.js

```js
// getRealtimeMetrics :: URL -> Stream Metric
export const getRealtimeMetrics = flow(
    connect,
    messages('metric')
);
```

Se pueden ver las diferencias implementadas [aquí](https://bitbucket.org/madoos1/tramitapp-front-challenge-3/compare/master%0Dchallenge#diff) 
